//
//  AppDelegate.m
//  GuardarInfo
//
//  Created by Francesc on 24/03/13.
//  Copyright (c) 2013 Francesc Tovar. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

@synthesize datos;
@synthesize ruta;

-(void) leer {
    self.datos = [[NSMutableDictionary alloc] initWithContentsOfFile:[self ruta]];
    if (self.datos) {
         NSLog(@"Los datos si existen: Usuario: %@ Password: %@", [self.datos objectForKey:@"usuario"],[self.datos objectForKey:@"password"]);
    } else {
        NSLog(@"Los datos no existen y se crea el diccionario");
        self.datos = [[NSMutableDictionary alloc] init];
    }
}

-(void) guardar {
    [self.datos writeToFile:[self ruta] atomically:YES];
     NSLog(@"Los datos se han guardado: Usuario: %@ Password: %@", [self.datos objectForKey:@"usuario"],[self.datos objectForKey:@"password"]);
}

-(NSString *) ruta {
    NSString *rutaCarpeta = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    return [rutaCarpeta stringByAppendingPathComponent:@"datos.plist"];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    [self leer];
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [self guardar];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [self guardar];
}

@end
