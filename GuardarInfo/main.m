//
//  main.m
//  GuardarInfo
//
//  Created by Francesc on 24/03/13.
//  Copyright (c) 2013 Francesc Tovar. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
