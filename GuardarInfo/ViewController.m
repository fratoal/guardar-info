//
//  ViewController.m
//  GuardarInfo
//
//  Created by Francesc on 24/03/13.
//  Copyright (c) 2013 Francesc Tovar. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize txtUsuario, txtPassword;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    UIApplication *aplicacion = [UIApplication sharedApplication];
    AppDelegate *delegado = (AppDelegate *)aplicacion.delegate;
    
    if ([delegado.datos objectForKey:@"usuario"]) {
        txtUsuario.text = [delegado.datos objectForKey:@"usuario"];
        txtPassword.text = [delegado.datos objectForKey:@"password"];
    }
    
}

- (IBAction)cambioUsuario:(id)sender {
    UIApplication *aplicacion = [UIApplication sharedApplication];
    AppDelegate *delegado = (AppDelegate *)aplicacion.delegate;
    [delegado.datos setObject:txtUsuario.text forKey:@"usuario"];
}

- (IBAction)cambioPassword:(id)sender {
    UIApplication *aplicacion = [UIApplication sharedApplication];
    AppDelegate *delegado = (AppDelegate *)aplicacion.delegate;
    [delegado.datos setObject:txtPassword.text forKey:@"password"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
