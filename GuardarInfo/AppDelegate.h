//
//  AppDelegate.h
//  GuardarInfo
//
//  Created by Francesc on 24/03/13.
//  Copyright (c) 2013 Francesc Tovar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSMutableDictionary *datos;
@property (strong, nonatomic) NSString *ruta;

-(void) leer;
-(void) guardar;
-(NSString *) ruta;

@end
