//
//  ViewController.h
//  GuardarInfo
//
//  Created by Francesc on 24/03/13.
//  Copyright (c) 2013 Francesc Tovar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

- (IBAction)cambioPassword:(id)sender;
- (IBAction)cambioUsuario:(id)sender;

@property (strong, nonatomic) IBOutlet UITextField *txtUsuario;
@property (strong, nonatomic) IBOutlet UITextField *txtPassword;

@end
